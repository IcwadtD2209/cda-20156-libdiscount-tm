package fr.afpa.libDiscount.beans;

import java.util.ArrayList;

public class ListeMembres {
	
private ArrayList <Utilisateur> listeMembres;
	
	public ListeMembres() {
		this.listeMembres = new ArrayList();
	}

	public ArrayList<Utilisateur> getListeMembres() {
		return listeMembres;
	}

	public void setListeMembres(ArrayList<Utilisateur> listeMembres) {
		this.listeMembres = listeMembres;
	}
	
}