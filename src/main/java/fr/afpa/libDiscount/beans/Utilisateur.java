package fr.afpa.libDiscount.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Configuration hibernate pour communiquer avec la base de données / Table Utilisateur
 */
	
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@NamedQuery(name="findUserByName", query="SELECT u FROM Utilisateur u WHERE u.nom = :valeur")
@NamedQuery(name="findUserByMail", query="SELECT u FROM Utilisateur u WHERE u.mail = :mail or u.mdp= :mdp")

@Entity
public class Utilisateur{
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "utilisateur_id_generator")
	@SequenceGenerator(name = "utilisateur_id_generator", sequenceName = "utilisateur_seq", allocationSize = 1, initialValue = 1)
	
	@Column(name="id_utilisateur")
	private int id_utilisateur;
	
	@Column(name="nom", nullable = false, length= 20, unique= true)
	private String nom;
	
	@Column(name="prenom", nullable = false, length= 20)
	private String prenom;
	
	@Column(name="librairie", nullable = false, length= 50)
	private String librairie;
	
	@Column(name="adresse", nullable = false, length= 50)
	private String adresse;
	
	@Column(name="mail", nullable = false, length= 50)
	private String mail;
	
	@Column(name="mdp", nullable = false, length= 20)
	private String mdp;
	
	@Column(name="telephone", nullable = false, length= 10)
	private String telephone;

	public Utilisateur(String nom, String prenom, String librairie, String adresse, String mail, String mdp, String telephone) {
		this.nom = nom;
		this.prenom = prenom;
		this.librairie = librairie;
		this.adresse = adresse;
		this.mail = mail;
		this.mdp = mdp;
		this.telephone = telephone;
	}
	
	@Override
	public String toString() {
		return "Nom : "+this.nom
				+"<br>Prenom : "+this.prenom
				+"<br>Librairie : "+this.librairie
				+"<br>Adresse : "+this.adresse
				+"<br>Email : "+this.mail
				+"<br>Telephone : "+this.telephone;
	}
	
}