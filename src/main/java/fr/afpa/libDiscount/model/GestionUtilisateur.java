package fr.afpa.libDiscount.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.libDiscount.beans.Utilisateur;
import fr.afpa.libDiscount.dao.DAOUtilisateur;
import fr.afpa.libDiscount.session.HibernateUtils;

public class GestionUtilisateur {
	
	private Session s;
	
	/** Méthode verifCo pour vérifier si l'utilisateur est valide
	 * @param utilisateur
	 * @return user
	 */
	
	public static Utilisateur verifCo(Utilisateur utilisateur) {
		
		DAOUtilisateur gu = new DAOUtilisateur();
		Utilisateur user = gu.checkValidUser(utilisateur);
		
		return user;
	}
	
	
	
	public ArrayList<Utilisateur> listerPersonnes(){
		// Ouverture d'une session Hibernate
		s = HibernateUtils.getSession();
		// Creation de la requete
		Query q = s.createQuery("from Utilisateur");

		ArrayList<Utilisateur> listUtilisateurs = (ArrayList<Utilisateur>) q.getResultList();
		return listUtilisateurs;
	}
	
}