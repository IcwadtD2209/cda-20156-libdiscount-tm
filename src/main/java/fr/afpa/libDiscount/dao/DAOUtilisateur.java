package fr.afpa.libDiscount.dao;

import java.util.ArrayList;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.libDiscount.beans.Utilisateur;
import fr.afpa.libDiscount.model.GestionUtilisateur;
import fr.afpa.libDiscount.session.HibernateUtils;

public class DAOUtilisateur {

	private Session s ;
	
	/**
	 * Méthode enregistrerUtilisateur permettant de créer un utilisateur en base de données
	 * @param Utilisateur user
	 */
	
	public void enregistrerUtilisateur(Utilisateur user) {
		// Creation d une session
		
		//Creation d une session hibernate
		s= HibernateUtils.getSession();
		//Debut de la transaction
		Transaction tx=s.beginTransaction();
		s.persist(user);
		tx.commit();
		s.close();
	}
	
	/**
	 * Méthode listerPersonnes permettant de lister l'ensemble des membres du site
	 * @return listUtilisateurs
	 */
	
	public ArrayList<Utilisateur> listerPersonnes() {
		// Ouverture d'une session Hibernate
		s = HibernateUtils.getSession();
		// Creation de la requete
		Query q = s.createQuery("from Utilisateur");

		ArrayList<Utilisateur> listUtilisateurs = (ArrayList<Utilisateur>) q.getResultList();
		return listUtilisateurs;
	}
	
	/**
	 * Methode recupUtilisateurParMail pour récupérer un utilisateur selon l'email renseigner lors de la création de son compte
	 * @param mail
	 * @return utilisateur
	 */
	
	public static Utilisateur recupUtilisateurParMail(String mail) {
		// Ouverture d'une session Hibernate
		Session	s = HibernateUtils.getSession();
				
		Query q = s.getNamedQuery("findUserByMail");
				
	    q.setParameter("mail", mail);
				
		Utilisateur utilisateur = (Utilisateur) q.getSingleResult();
				
		s.close();
		return utilisateur;
	}
	
	/**
	 * Methode pour vérifier l'existence de l'utilisateur grâce aux mail et mdp tapés
	 * @param utilisateur
	 * @return user
	 */
		
	public Utilisateur checkValidUser(Utilisateur utilisateur) {
		
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();

		Query qMail = s.getNamedQuery("findUserByMail");
		qMail.setParameter("mail", utilisateur.getMail());
		qMail.setParameter("mdp", utilisateur.getMdp());

		if (qMail.getResultList().size()==1) {
			Utilisateur user = ( (Utilisateur) qMail.getResultList().get(0));
		
			return user;
		}
		
		tx.commit();
		return null;
	}
}