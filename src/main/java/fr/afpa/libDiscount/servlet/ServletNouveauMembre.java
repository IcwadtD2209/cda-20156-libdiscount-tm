package fr.afpa.libDiscount.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.libDiscount.beans.Utilisateur;
import fr.afpa.libDiscount.dao.DAOUtilisateur;
import fr.afpa.libDiscount.model.GestionUtilisateur;

/**
 * Servlet implementation class ServletNouveauMembre
 */
public class ServletNouveauMembre extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nom = request.getParameter("name");
		String prenom = request.getParameter("firstname");
		String librairie = request.getParameter("libname");
		String adresse = request.getParameter("address");
		String mail = request.getParameter("mail");
		String mdp = request.getParameter("mdp");
		String telephone = request.getParameter("phone");

		Utilisateur u = new Utilisateur(nom, prenom, librairie, adresse, mail, mdp, telephone);
		
		new DAOUtilisateur().enregistrerUtilisateur(u);
		
		request.setAttribute("user", u);
		RequestDispatcher dispatcher = request.getRequestDispatcher("Confirmation.jsp");
		dispatcher.forward(request, response);

	}
}