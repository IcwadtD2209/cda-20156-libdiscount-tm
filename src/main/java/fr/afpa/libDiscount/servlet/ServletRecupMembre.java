package fr.afpa.libDiscount.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.libDiscount.beans.ListeMembres;
import fr.afpa.libDiscount.beans.Utilisateur;
import fr.afpa.libDiscount.model.GestionUtilisateur;

/**
 * Servlet implementation class ServletRecupMembre
 */
public class ServletRecupMembre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<Utilisateur> listMembres = new GestionUtilisateur().listerPersonnes();
		for (Utilisateur utilisateur : listMembres) {
			request.setAttribute("liste", listMembres);
			RequestDispatcher dispatcher = request.getRequestDispatcher("AfficherMembres.jsp");
			dispatcher.forward(request, response);
		}
	}
	
}