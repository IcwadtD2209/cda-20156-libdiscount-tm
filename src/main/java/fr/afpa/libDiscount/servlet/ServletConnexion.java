package fr.afpa.libDiscount.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.libDiscount.beans.Utilisateur;
import fr.afpa.libDiscount.model.GestionUtilisateur;

/**
 * Servlet implementation class ServletConnexion
 */
public class ServletConnexion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String mail = request.getParameter("mail");
		String mdp = request.getParameter("mdp");
		
		Utilisateur user = new Utilisateur("", "", "", "", mail, mdp, "") ;
		
		GestionUtilisateur gu = new GestionUtilisateur();
		
		Utilisateur theUser = gu.verifCo(user);
		if (theUser!=null) {
			
			HttpSession session = request.getSession(true);
			
			session.setAttribute("userCo", theUser);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("AuthOK.jsp");
			dispatcher.forward(request, response);
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("AuthKO.jsp");
			dispatcher.forward(request, response);
		}
	}

}
