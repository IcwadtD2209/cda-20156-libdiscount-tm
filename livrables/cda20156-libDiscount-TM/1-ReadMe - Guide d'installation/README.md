cda20156-libDiscount-TM
_________________________________________________________________________________________
Mini-projet cda20156-libDiscount-TM
16/11/2020 - Roubaix.


Finalité de l'application :
_________________________________________________________________________________________
Création d'une application qui permet de mettre en relation des libraires afin de poster
des annonces de leurs produits pour écouler leurs stocks de livres.


Pour la partie "gestion d'utilisateurs", l'application doit pouvoir proposer les 
fonctionnalités suivantes :
- s'enregistrer sur le site (créer un compte utilisateur)
- se connecter grâce aux mail et mot de passe renseignés lors de la création
- lister l'ensemble des membres
- modifier les membres
- supprimer les membres (sauf soi-même)

Suivi du projet :
_________________________________________________________________________________________
Le tableau de bord du projet est disponible sur le lien suivant :
https://trello.com/b/ECmQdYiP/cda20156-libdiscount-tm


Conception du projet :
_________________________________________________________________________________________
Les diagrammes UML (de cas d'utilisation et de séquence) sont également
disponibles sur le tableau de bord Trello.


Démarche suivie :
_________________________________________________________________________________________
Lecture du projet et assimilation des concepts attendus.
Compréhension du projet.
Conception des diagrammes UML de cas d'utilisation et de séquence.
Création d'un tableau de bord Trello.
Création du repository et partage via BitBucket.
Projet mis en place sur GitKraken.


Pré-requis :
________________________________________________________________________________________
Récupérer la source du projet disponible sous :
livrables\cda20156-libDiscount-TM\6- Code source\cda-20156-libdiscount-tm
Ouvrir Eclipse ou un autre IDE disposant d'un serveur Apache.
Lancer le projet sur le serveur.

Créer une base de données PostgreSQL, la nommer "cda-20156-libDiscount-TM" avec un 
utilisateur disposant des droits super user s'appelant "admin" avec le mot de passe "admin"


Fonctionnalités disponibles :
________________________________________________________________________________________
- accès à la page index.html
- s'enregistrer comme membre grâce au formulaire
- à l'enregistrement, à la connexion.


Contributeur :
_________________________________________________________________________________________
Thomas Mazzini

©TM - 2020